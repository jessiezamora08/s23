// db.users.insertOne({
    //     "firstName": "John",
    //     "lastName": "Smith"
    // });
    //
    // db.users.insertMany( [
    // {"firstName":"John", "lastName": "Doe" },
    // {"firstName": "Jane", "lastName": "Doe"}
    //
    // ]);
    //
    // db.users.find();
    //
    // db.users.find({"lastName": "Doe"});
    //
    // db.users.updateOne(
    //     {
    //         "_id":ObjectId("61d592c74b14a159f0a96ea5")
    //     },
    //     {
    //         $set: {
    //             "email": "johnsmith@gmail.com"
    //         }
    //     }
    // );
    
    // db.users.updateMany(
    //     {
    //         "lastName": "Doe"
    //     },
    //     {
    //         $set: {
    //             "isAdmin": false
    //         }
    //     }
    // );
    
    // db.users.deleteMany({"lastName" : "Doe"});
    // db.users.deleteOne({"_id": ObjectId("61d593b64b14a159f0a96ea7")});


   // INSERT A SINGLE ROOM 
    db.rooms.insertOne({ "name": "single", "accommodates": "2",  "price": 1000, "description": "A simple room with all the basic necessities", 
    "rooms_available": 10, "isAvailable": false  });

// INSERT MULTIPLE ROOMS 

db.users.insertMany( [

    {"name": "double", "accommodates": "3",  "price": 2000, "description": "A room fit for vacation", 
    "rooms_available": 5, "isAvailable": false },
    {"name": "queen", "accommodates": "4",  "price": 4000, "description": "A room for family", 
    "rooms_available": 15, "isAvailable": false },
    
    
]);


//SEARCH FOR A ROOM 

db.users.find({"name": "double"});


//UPDATE QUEEN ROOM:

db.users.updateOne(
  {
    _id: ObjectId("61d6c1376b214505289567a5"),
  },
  {
    $set: {
      rooms_available: 0,
    },
  }
);

// DELETE ROOMS


db.users.deleteMany({"rooms_available" : 0});